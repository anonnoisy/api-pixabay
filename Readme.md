# Aplikasi Pixabay
### Menggunakan API Pixabay menampilkan dengan RecyclerView

#### Deskripsi Aplikasi

Aplikasi ini menggunakan API dari Pixabay, Aplikais ini hanya menampilkan Gambar, Nama Creator, Gambar Creator, Likes.

Aplikasi ini menampilkan gambar dengan menggunakan widget RecyclerView.

#### Gradle

<pre><code>
dependencies {
               implementation 'com.android.support:design:26.1.0'
               implementation 'com.android.support:recyclerview-v7:26.1.0'
               implementation 'com.android.support:cardview-v7:26.1.0'
               implementation 'com.android.volley:volley:1.1.0'
               implementation 'com.squareup.picasso:picasso:2.5.2'
               implementation 'de.hdodenhof:circleimageview:2.2.0'
               }
</code></pre>

Aplikasi ini juga menggunakan dependcy dari *https://github.com/hdodenhof/CircleImageView
Depedency ini digunakan untuk membuat gambar menjadi lingkaran

Untuk download data dari Pixabay yang barupa JSON, Aplikasi ini menggunakan volley, 
sedangkan untuk download data gambar Aplikasi ini menggunakan Picasso


#### Link APK

> https://drive.google.com/drive/u/0/folders/1Tl6AQ79ej37Qy6qi2Ur92OSgT-QJXlPl