package id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by batuh on 09/02/2018.
 */

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ListItem> mListItems;
    private OnItemClickListener mListener;

    public ListItemAdapter(Context context, ArrayList<ListItem> listItems) {
        this.mContext = context;
        this.mListItems = listItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem currentItem = mListItems.get(position);

        String imageUrl = currentItem.getmImageUrl();
        String imageCreator = currentItem.getmImageCreator();
        String creatorName = currentItem.getmCreator();
        int likeCount = currentItem.getmLike();

        holder.mTextViewCreator.setText(creatorName);
        holder.mTextViewLikes.setText("Likes: " + likeCount);
        Picasso.with(mContext).load(imageUrl).fit().centerInside().into(holder.mImageView);
        Picasso.with(mContext).load(imageCreator).fit().centerInside().into(holder.mImageCreator);
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    public void setOnClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView mImageView;
        public ImageView mImageCreator;
        public TextView mTextViewCreator;
        public TextView mTextViewLikes;

        public ViewHolder(View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.image_view);
            mImageCreator = itemView.findViewById(R.id.image_view_creator);
            mTextViewCreator = itemView.findViewById(R.id.tv_creator);
            mTextViewLikes = itemView.findViewById(R.id.tv_like);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
