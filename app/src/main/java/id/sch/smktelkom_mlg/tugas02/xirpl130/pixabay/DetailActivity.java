package id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay.MainActivity.EXTRA_CREATOR;
import static id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay.MainActivity.EXTRA_IMAGE;
import static id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay.MainActivity.EXTRA_IMAGE_CREATOR;
import static id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay.MainActivity.EXTRA_LIKES;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra(EXTRA_IMAGE);
        String imageCreator = intent.getStringExtra(EXTRA_IMAGE_CREATOR);
        String creatorName = intent.getStringExtra(EXTRA_CREATOR);
        int likeCount = intent.getIntExtra(EXTRA_LIKES, 0);

        ImageView imageView = findViewById(R.id.image_view_detail);
        ImageView imageViewCreator = findViewById(R.id.image_view_detail_creator);
        TextView textViewCreator = findViewById(R.id.text_view_creator_detail);
        TextView textViewLikes = findViewById(R.id.text_view_like_detail);

        Picasso.with(this).load(imageUrl).fit().centerInside().into(imageView);
        Picasso.with(this).load(imageCreator).fit().centerInside().into(imageViewCreator);
        textViewCreator.setText(creatorName);
        textViewLikes.setText("Likes: " + likeCount);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
