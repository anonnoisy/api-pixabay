package id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay;

/**
 * Created by batuh on 09/02/2018.
 */

public class ListItem {

    private String mImageUrl;
    private String mImageCreator;
    private String mCreator;
    private int mLike;

    public ListItem(String mImageUrl, String mImageCreator, String mCreator, int mLike) {
        this.mImageUrl = mImageUrl;
        this.mImageCreator = mImageCreator;
        this.mCreator = mCreator;
        this.mLike = mLike;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public String getmImageCreator() {
        return mImageCreator;
    }

    public String getmCreator() {
        return mCreator;
    }

    public int getmLike() {
        return mLike;
    }
}
