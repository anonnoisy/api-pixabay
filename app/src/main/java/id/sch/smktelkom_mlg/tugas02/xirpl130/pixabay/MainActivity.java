package id.sch.smktelkom_mlg.tugas02.xirpl130.pixabay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ListItemAdapter.OnItemClickListener {

    public static final String EXTRA_IMAGE = "imageUrl";
    public static final String EXTRA_IMAGE_CREATOR = "imageCreator";
    public static final String EXTRA_CREATOR = "creatorName";
    public static final String EXTRA_LIKES = "likeCount";

    String url = "https://pixabay.com/api/?key=7983008-fb93e13e1f8d155c08cc0307d&q=yellow+flowers&image_type=photo&pretty=true";
    private RecyclerView mRecyclerView;
    private ListItemAdapter mListItemAdapter;
    private ArrayList<ListItem> mListItem = new ArrayList<>();
    private RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRequestQueue = Volley.newRequestQueue(this);

        parseJSON();

    }

    private void parseJSON() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("hits");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject hit = jsonArray.getJSONObject(i);

                                String creatorName = hit.getString("user");
                                String imageUrl = hit.getString("webformatURL");
                                String imageCreator = hit.getString("userImageURL");
                                int likeCount = hit.getInt("likes");

                                mListItem.add(new ListItem(imageUrl, imageCreator, creatorName, likeCount));
                            }

                            mListItemAdapter = new ListItemAdapter(MainActivity.this, mListItem);
                            mRecyclerView.setAdapter(mListItemAdapter);
                            mListItemAdapter.setOnClickListener(MainActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(request);
    }

    @Override
    public void onItemClick(int position) {
        Intent detailIntent = new Intent(this, DetailActivity.class);
        ListItem clickedItem = mListItem.get(position);

        detailIntent.putExtra(EXTRA_IMAGE, clickedItem.getmImageUrl());
        detailIntent.putExtra(EXTRA_IMAGE_CREATOR, clickedItem.getmImageCreator());
        detailIntent.putExtra(EXTRA_CREATOR, clickedItem.getmCreator());
        detailIntent.putExtra(EXTRA_LIKES, clickedItem.getmLike());

        startActivity(detailIntent);
    }

}
